package com.almworks.integers.generator;

public class SkipImportsRegex {
  public static final String DOT = "\\s*[.]\\s*";
  public static final String I = "import\\s+(?:static\\s+)?((?:\\w|#)+(?:"  + DOT + "(?:\\w|#)+)*(?:" + DOT + "[*])?)\\s*;";
  public static final String J = "(?:" + I + "(?:\\s*" + I + ")*)";
  public static final String K = "(?m:\\/\\/\\s*" + J + "\\s*?)";
  public static final String L = "(?:\\/[*]\\s*" + J + "\\s*[*]\\/)";
  public static final String M = "(?:" + J + "|" + K + "|" + L+ ")";
  public static final String P = "(?:package\\s+(?:\\w|#)+(?:" + DOT + "(?:\\w|#)+)*\\s*);";
  public static final String N = "(?:" + P + "?(\\s*(?:" + M + "(?:\\s*" + M + ")*)\\s*))";

  public static final String REGEX = N;
  public static final String WHITESPACE_REGEX = P + "?(\\s*)";
}
