package com.almworks.util;

import java.io.*;

public class FileUtil {
  public static void writeFile(File file, String content, String charsetName) throws IOException {
    byte[] bytes = content.getBytes(charsetName);
    try (OutputStream stream = new FileOutputStream(file)) {
      stream.write(bytes);
    }
  }

  public static String readFile(File file, String charsetName) throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    byte[] buffer = new byte[8192];
    try (InputStream stream = new FileInputStream(file)) {
      int read;
      while ((read = stream.read(buffer)) >= 0) {
        bytes.write(buffer, 0, read);
      }
    }
    return new String(bytes.toByteArray(), charsetName);
  }
}
