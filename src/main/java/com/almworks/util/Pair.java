package com.almworks.util;

import java.util.Objects;

public abstract class Pair<T1, T2> {
  public abstract T1 getFirst();

  public abstract T2 getSecond();

  public int hashCode() {
    T1 first = getFirst();
    int code = first == null ? 9010391 : first.hashCode();
    T2 second = getSecond();
    code = code * 23 + (second == null ? 28719051 : second.hashCode());
    return code;
  }

  @SuppressWarnings({"RawUseOfParameterizedType"})
  public boolean equals(Object o) {
    if (!(o instanceof Pair))
      return false;
    Object thatFirst = ((Pair) o).getFirst();
    Object thatSecond = ((Pair) o).getSecond();
    return Objects.equals(thatFirst, getFirst()) && Objects.equals(thatSecond, getSecond());
  }

  public String toString() {
    return "Pair<" + getFirst() + ", " + getSecond() + ">";
  }

  public static <T1, T2> Pair<T1, T2> create(final T1 first, final T2 second) {
    return new Pair<T1, T2>() {
      public T1 getFirst() {
        return first;
      }

      public T2 getSecond() {
        return second;
      }
    };
  }
}
