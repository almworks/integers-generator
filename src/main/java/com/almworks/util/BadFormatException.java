package com.almworks.util;

public class BadFormatException extends Exception {
  public BadFormatException(String message, Throwable cause) {
    super(message, cause);
  }

  public BadFormatException(String message) {
    super(message);
  }

  public BadFormatException() {
    super();
  }
}
